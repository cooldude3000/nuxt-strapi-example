import { gtmActions } from '~/actions/gtm.js'
import { documentsActions } from '~/actions/documents.js'
import userPlugins from '~/store-plugins/user.js'
import { syncStorePlugin } from '~/store-plugins/sync-store.js'
import { INITIAL_STATE, SYNCED_STATE_PROPERTIES } from '~/config/store.js'
import { getTitleAndReference } from '~/helpers/formatting.js'

export const plugins = [userPlugins, syncStorePlugin(SYNCED_STATE_PROPERTIES)]

export const state = () => INITIAL_STATE

export const getters = {
  signedIn: ({ signedIn }) => signedIn,
  user: ({ user }) => user,
  savedDocuments: ({ savedDocuments }) => savedDocuments,
  savedDocumentNodeIds: ({ savedDocuments }) => savedDocuments ? savedDocuments.map(({ node_id }) => node_id) : [], // eslint-disable-line
  documents: ({ documents }) => documents,
  recentlyViewedDocuments: ({ recentlyViewedDocuments }) => recentlyViewedDocuments,
  searchResults: ({ searchResults }) => searchResults,
}

export const mutations = {
  signedIn: (state, { signedIn }) => {
    state.signedIn = signedIn
  },
  user: (state, { user }) => {
    state.user = user
  },
  savedDocuments: (state, { savedDocuments }) => {
    state.savedDocuments = savedDocuments
  },
  documents: (state, { documents }) => {
    state.documents = documents
  },
  recentlyViewedDocuments: (state, { recentlyViewedDocuments }) => {
    state.recentlyViewedDocuments = recentlyViewedDocuments
  },
  searchResults: (state, { searchResults }) => {
    state.searchResults = searchResults
  },

  // Reducers
  addRecentlyViewedDocuments: (state, payload) => {
    state.recentlyViewedDocuments = state.recentlyViewedDocuments.reduce((acc, doc, i) => {
      // If an item is wrongly formatted, remove it...
      if (!doc.nodeId || !doc.title) return acc

      // If this doc is already included don't add it again...
      if (acc.some(({ nodeId }) => doc.nodeId === nodeId)) {
        return acc
      }

      // If recentlyViewedDocuments is longer than 6 items remove the oldest
      // one...
      if (i > 5) {
        return [
          ...acc.slice(0, acc.length - 1),
          doc,
        ]
      }

      // Fill the array normally...
      return [ ...acc, doc ]
    }, [{
      // This is the new document being added to the list...
      title: getTitleAndReference(payload.document),
      nodeId: payload.document?.NODE_ID,
      version: payload.document?.REVISION,
    }])
  },
}

export const actions = {
  ...gtmActions,
  ...documentsActions,
}
