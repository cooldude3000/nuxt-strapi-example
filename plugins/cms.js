import { jsonAPINormaliser } from '~/helpers/jsonAPINormaliser.js'
import { API_PATH } from '~/config/paths.js'

export default function(ctx, inject) {
  const cms = ctx.$axios.create()
  cms.setBaseURL(process.env.cmsApiUrl + API_PATH)
  cms.interceptors.response.use(({ data }) => jsonAPINormaliser({ data }))
  ctx.$cms = cms
  inject('cms', cms)
}
