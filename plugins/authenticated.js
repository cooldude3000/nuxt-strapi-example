import { jsonAPINormaliser } from '~/helpers/jsonAPINormaliser.js'

export default function(ctx, inject) {
  const authenticated = ctx.$axios.create({
    baseURL: '/authenticated',
  })
  authenticated.interceptors.response.use(({ data }) => {
    // Convert Strapi v4 data to be backwards compatible
    return jsonAPINormaliser({ data })
  }, error => {
    // If the response is 401 this means user's unauthenticated
    if (error.response?.status === 401) {
      ctx.store.commit('signedIn', { signedIn: false })
    }
    return error
  })
  ctx.$authenticated = authenticated
  inject('authenticated', authenticated)
}

