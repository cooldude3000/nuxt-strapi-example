require('dotenv').config()

export default function({ $gtm, route }) {
  function gtag() { $gtm.push(arguments) }
  gtag('consent', 'default', {
    ad_storage: 'denied',
    analytics_storage: 'denied',
  });

  $gtm.init(process.env.GTM_ID)
}
