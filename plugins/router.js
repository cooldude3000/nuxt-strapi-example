export default ({ app, store }) => {
  app.router.afterEach((to, from, next) => {
    store.dispatch('routeChange', to)
  })
}
