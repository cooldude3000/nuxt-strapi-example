export default function ({ $axios }, inject) {
  const tses = $axios.create({
    baseURL: '/tses',
    browserBaseURL: '/tses',
  })

  inject('tses', tses)
}
