import { SYNCED_STATE_PROPERTIES } from '../config/store.js'
import { replaceStateWithLocalStore } from '../store-plugins/sync-store.js'

export default ({ store }) => replaceStateWithLocalStore(store, SYNCED_STATE_PROPERTIES)
