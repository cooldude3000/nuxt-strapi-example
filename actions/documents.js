import { INITIAL_STATE } from '../config/store.js'
import { hasHtml, attachmentUrl, htmlDocumentLink } from '../lib/caci_api_helpers.js'
import {
  disciplineFormatted,
  lifecycleStageFormatted,
  issueDateFormatted,
  isWithdrawn,
  sortByCreatedAtDesc,
} from '~/helpers/formatting.js'


export const documentsActions = {
  async getSavedDocuments({ commit, state }) {
    const { user } = state

    try {
      const { data: documents } = await this.$authenticated('saved-documents/details/' + user.id)

      if (documents.length === 0) {
        commit('documents', { documents: INITIAL_STATE.documents })
        commit('savedDocuments', { documents: INITIAL_STATE.savedDocuments })
        return
      }

      commit('savedDocuments', { savedDocuments: documents.map(({ savedDocument }) => savedDocument) })

      commit('documents', {
        documents: documents.map(({ resultData, savedDocument }) => {
          const { title, createdAt } = savedDocument
          const {
            NODE_ID,
            DATE_OF_WITHDRAWAL,
            DISCIPLINE,
            SUITE,
            REVISION,
            DATE_OF_ISSUE,
            LIFECYCLE_STAGE,
            // TODO the normaliser is converting some arrays into indexed
            // objects, fix that at some point...
            ATTACHMENTS: attachmentsNonArr,
          } = resultData
          const ATTACHMENTS = Object.entries(attachmentsNonArr).map(ent => ent[1])
          return {
            title,
            createdAt,
            lifecycleStage: lifecycleStageFormatted(LIFECYCLE_STAGE),
            issued: issueDateFormatted(DATE_OF_ISSUE),
            revision: REVISION,
            from: SUITE,
            discipline: disciplineFormatted(DISCIPLINE),
            isWithdrawn: isWithdrawn(DATE_OF_WITHDRAWAL),
            documentNodeId: NODE_ID,
            documentPageUrl: `/search/${NODE_ID}`,
            htmlDocumentLink: hasHtml(ATTACHMENTS)
              ? htmlDocumentLink(NODE_ID, SUITE)
              : undefined,
            attachmentUrl: attachmentUrl(NODE_ID),
          }
        })
        .sort(sortByCreatedAtDesc)
      })

    } catch (error) {
      console.log(error)
    }

  },
  async addSavedDocument({ state, dispatch }, { documentNodeId, title }) {
    const { user } = state

    try {
      await this.$authenticated
        .post(
          '/saved-documents', {
            data: {
              title,
              node_id: documentNodeId,
              // Make sure you have the "find" and "update" permissions set on
              // the User for this work...
              users_permissions_user: user.id,
            }
          }
        )

    } catch (error) {
      console.log(error)
    }

    dispatch('getSavedDocuments')
  },
  async removeSavedDocument({ state, dispatch }, { documentNodeId }) {
    const { savedDocuments } = state
    const id = savedDocuments.find(doc => doc.node_id === documentNodeId)?.id

    try {
      if (!id) {
        const error = new Error(`Can't find document with node_id: ${documentNodeId}`)
        dispatch('error', error)
        throw error
      }

      await this.$authenticated
        .delete('/saved-documents/' + id)

    } catch (error) {
      console.log(error)
    }

    dispatch('getSavedDocuments')
  },
  onSearchResultCardSaveDocument({ state, getters, dispatch }, { documentNodeId, title, saved }) {
    if (!saved) {
      dispatch('removeSavedDocument', { documentNodeId })
    } else {
      dispatch('addSavedDocument', { documentNodeId, title })
      dispatch('savedDocument', { documentNodeId })
    }
  },
  onBookmarkCardRemove({ dispatch, state }, { documentNodeId }) {
    dispatch('removeSavedDocument', { documentNodeId })
  },
  myAccountBeforeMount({ dispatch }) {
    dispatch('getSavedDocuments')
  },
  searchNodeIdBeforeMount({ commit }, { document }) {
    commit('addRecentlyViewedDocuments', { document })
  },
}

