import { variables, events } from '../config/gtm'

const {
  SEARCH_TERM,
  DOWNLOAD_TRACKING,
  SAVED_DOCUMENTS,
  ERROR_MESSAGE,
  PAGE_TYPE,
  SIGN_IN,
  STANDARDS_DOWNLOAD,
  DOCUMENT_OPENED,
  DOCUMENT_DOWNLOAD,
} = events

// All these functions are set to match our SEO gtm requirements
export const eventVariableDefaults = {
  [SEARCH_TERM]: ({ name, ref }) => {
    const { $el, id } = ref
    return {
      [variables.SEARCH_TERM]: $el?.querySelector('input[type=search]')?.value,
      [variables.PAGE_TYPE]: pageType(name),
      [variables.FORM_ID]: id,
      [variables.CLICK_TEXT]: null,
    }
  },
  [DOWNLOAD_TRACKING]: (payload) => ({
    [variables.CLICK_TEXT]: null,
    [variables.PAGE_TYPE]: null,
    [variables.DOC_STANDARD]: null,
    [variables.VERSION_NUMBER]: null,
  }),
  [SAVED_DOCUMENTS]: ({ TITLE, SUITE, REVISION, DATE_OF_ISSUE }) => ({
    [variables.DOC_NAME]: TITLE,
    [variables.PAGE_TYPE]: null,
    [variables.DOC_STANDARD]: SUITE,
    [variables.DOC_REVISION]: REVISION,
    [variables.ISSUED]: DATE_OF_ISSUE,
    [variables.SEARCH_MATCH]: null,
  }),
  [ERROR_MESSAGE]: ({ message }) => ({
    [variables.ERROR_MESSAGE]: message,
  }),
  [SIGN_IN]: ({ ref, name }) => {
    const { id } = ref
    return {
      [variables.PAGE_TYPE]: name,
      [variables.FORM_ID]: id,
    }
  },
  [PAGE_TYPE]: ({ name }) => ({
    [variables.PAGE_TYPE]: pageType(name),
  }),
  [STANDARDS_DOWNLOAD]: ({ name, validAt, standards, clickText }) => ({
    [variables.CLICK_TEXT]: clickText,
    [variables.PAGE_TYPE]: pageType(name),
    [variables.DOC_STANDARD]: standards.join(' '),
    [variables.DATE]: validAt,
  }),
  [DOCUMENT_DOWNLOAD]: ({ name, document, clickText }) => ({
    [variables.PAGE_TYPE]: pageType(name),
    [variables.CLICK_TEXT]: clickText,
    [variables.DOC_TYPE]: null,
    [variables.DOC_STANDARD]: document.SUITE,
    [variables.DOC_REVISION]: document.REVISION,
    [variables.ISSUED]: document.DATE_OF_ISSUE,
    [variables.DOC_NAME]: document.TITLE,
  }),
  [DOCUMENT_OPENED]: ({ name, document, clickText }) => ({
    [variables.PAGE_TYPE]: pageType(name),
    [variables.CLICK_TEXT]: clickText,
    [variables.DOC_TYPE]: null,
    [variables.DOC_STANDARD]: document.SUITE,
    [variables.DOC_REVISION]: document.REVISION,
    [variables.ISSUED]: document.DATE_OF_ISSUE,
    [variables.DOC_NAME]: document.TITLE,
  }),
}

const pageType = name => name === 'index' ? 'homepage' : name

export const gtmActions = {
  gtmPush(_, payload) {
    this.$gtm.push(payload)
  },

  headerSearchOnSubmitSearch({ dispatch }, payload) {
    dispatch('gtmPush', {
      event: SEARCH_TERM,
      ...eventVariableDefaults[SEARCH_TERM](payload),
    })
  },
  homepageOnSubmitSearch({ dispatch }, payload) {
    dispatch('gtmPush', {
      event: SEARCH_TERM,
      ...eventVariableDefaults[SEARCH_TERM](payload),
    })
  },
  searchIndexOnSubmitSearch({ dispatch }, payload) {
    const { ref } = payload
    const { $attrs } = ref
    dispatch('gtmPush', {
      event: SEARCH_TERM,
      ...eventVariableDefaults[SEARCH_TERM](payload),
      [variables.FORM_ID]: $attrs.id,
    })
  },
  signIn({ dispatch }, payload) {
    dispatch('gtmPush', {
      event: SIGN_IN,
      ...eventVariableDefaults[SIGN_IN](payload),
    })
  },
  routeChange({ dispatch }, payload) {
    dispatch('gtmPush', {
      event: PAGE_TYPE,
      ...eventVariableDefaults[PAGE_TYPE](payload),
    })
  },
  error({ dispatch }, payload) {
    console.log('Error:', payload)
    dispatch('gtmPush', {
      event: ERROR_MESSAGE,
      ...eventVariableDefaults[ERROR_MESSAGE](payload),
    })
  },
  savedDocument({ dispatch, state }, { documentNodeId }) {
    const { page } = state.searchResults
    const {
      resultData,
      formattedData,
      matchType,
    } = page?.find(d => d.nodeId === documentNodeId)

    dispatch('gtmPush', {
      event: SAVED_DOCUMENTS,
      ...eventVariableDefaults[SAVED_DOCUMENTS]({ ...resultData, ...formattedData }),
      [variables.PAGE_TYPE]: pageType(this.$router.app.$route.name),
      [variables.SEARCH_MATCH]: matchType,
    })
  },
  downloadStandardsOnProcessDownload({ dispatch, state }, payload) {
    dispatch('gtmPush', {
      event: STANDARDS_DOWNLOAD,
      ...eventVariableDefaults[STANDARDS_DOWNLOAD](payload),
    })
  },
  searchNodeIdOnOpenPdf({ dispatch }, payload) {
    dispatch('gtmPush', {
      event: DOCUMENT_OPENED,
      ...eventVariableDefaults[DOCUMENT_OPENED](payload),
    [variables.DOC_TYPE]: 'PDF',
    })
  },
  searchNodeIdOnDownloadPdf({ dispatch }, payload) {
    dispatch('gtmPush', {
      event: DOCUMENT_DOWNLOAD,
      ...eventVariableDefaults[DOCUMENT_DOWNLOAD](payload),
    [variables.DOC_TYPE]: 'PDF',
    })
  },
  searchNodeIdOnOpenHtml({ dispatch }, payload) {
    dispatch('gtmPush', {
      event: DOCUMENT_OPENED,
      ...eventVariableDefaults[DOCUMENT_OPENED](payload),
      [variables.DOC_TYPE]: 'HTML',
    })
  },
  searchResultsCardOnOpenPDF({ dispatch, state }, { name, documentNodeId, clickText }) {
    const document = state.searchResults.page.find(d => d.nodeId === documentNodeId)
    const { formattedData, resultData } = document
    if (!document) {
      console.error('GTM error: No document found with nodeId ', documentNodeId)
      return
    }
    dispatch('gtmPush', {
      event: DOCUMENT_OPENED,
      ...eventVariableDefaults[DOCUMENT_OPENED]({ name, clickText, document: { ...resultData, ...formattedData }}),
    [variables.DOC_TYPE]: 'PDF',
    })
  },
  searchResultsCardOnOpenHTML({ dispatch, state }, { name, documentNodeId, clickText }) {
    const document = state.searchResults.page.find(d => d.nodeId === documentNodeId)
    if (!document) {
      console.error('GTM error: No document found with nodeId ', documentNodeId)
      return
    }
    const { formattedData, resultData } = documentNodeId
    dispatch('gtmPush', {
      event: DOCUMENT_OPENED,
      ...eventVariableDefaults[DOCUMENT_OPENED]({ name, clickText, document: { ...resultData, ...formattedData }}),
    [variables.DOC_TYPE]: 'HTML',
    })
  },
  searchResultsCardOnDownload({ dispatch, state }, { name, documentNodeId, clickText }) {
    const document = state.searchResults.page.find(d => d.nodeId === documentNodeId)
    if (!document) {
      console.error('GTM error: No document found with nodeId ', documentNodeId)
      return
    }
    const { formattedData, resultData } = document
    dispatch('gtmPush', {
      event: DOCUMENT_DOWNLOAD,
      ...eventVariableDefaults[DOCUMENT_DOWNLOAD]({ name, clickText, document: { ...resultData, ...formattedData }}),
    [variables.DOC_TYPE]: 'PDF',
    })
  },
  bookmarkCardOnOpenPdf({ dispatch, state }, { name, clickText, documentNodeId }) {
    const document = state.searchResults.page.find(d => d.nodeId === documentNodeId)
    if (!document) {
      console.error('GTM error: No document found with nodeId ', documentNodeId)
      return
    }
    const { formattedData, resultData } = document
    dispatch('gtmPush', {
      event: DOCUMENT_OPENED,
      ...eventVariableDefaults[DOCUMENT_OPENED]({ name, clickText, document: { ...resultData, ...formattedData }}),
      [variables.DOC_TYPE]: 'PDF',
    })
  },
  bookmarkCardOnOpenHtml({ dispatch, state }, { name, clickText, documentNodeId }) {
    const document = state.searchResults.page.find(d => d.nodeId === documentNodeId)
    if (!document) {
      console.error('GTM error: No document found with nodeId ', documentNodeId)
      return
    }
    const { formattedData, resultData } = document
    dispatch('gtmPush', {
      event: DOCUMENT_OPENED,
      ...eventVariableDefaults[DOCUMENT_OPENED]({ name, clickText, document: { ...resultData, ...formattedData }}),
      [variables.DOC_TYPE]: 'HTML',
    })
  },
  bookmarkCardOnDownloadPdf({ dispatch, state }, { name, clickText, documentNodeId }) {
    const document = state.searchResults.page.find(d => d.nodeId === documentNodeId)
    if (!document) {
      console.error('GTM error: No document found with nodeId ', documentNodeId)
      return
    }
    const { formattedData, resultData } = document
    dispatch('gtmPush', {
      event: DOCUMENT_DOWNLOAD,
      ...eventVariableDefaults[DOCUMENT_DOWNLOAD]({ name, clickText, document: { ...resultData, ...formattedData }}),
      [variables.DOC_TYPE]: 'PDF',
    })
  },
}
