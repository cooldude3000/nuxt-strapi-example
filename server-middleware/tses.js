const axios = require('axios')
const express = require('express')
const app = express()
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

require('dotenv').config()

const { API_USERNAME, API_PASSWORD, API_ENDPOINT } = process.env

const tses = axios.create({
  baseURL: API_ENDPOINT,
  ...(API_USERNAME && API_PASSWORD ? {
    auth: {
      username: API_USERNAME,
      password: API_PASSWORD,
    }
  } : {}),
})

app.get('/attachments/*', async (req, res) => {
  const { url } = req
  try {
    const response = await tses(url, { responseType: 'arraybuffer' })
    const { headers, data } = response
    res.set(headers)
    res.send(data)
  } catch (error) {
    res.status(error.response.status).send(error.message)
  }
})

app.get('/*', async (req, res) => {
  const { url } = req
  try {
    const { data } = await tses(url)
    res.send(data)
  } catch (error) {
    res.status(error.response.status).send(error.message)
  }
})

module.exports = app
