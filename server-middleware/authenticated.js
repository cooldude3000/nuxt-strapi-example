const axios = require('axios')
const express = require('express')
const cookieParser = require('cookie-parser')
const { COOKIE_NAME } = require('../config/cookies.js')

const app = express()
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cookieParser())

require('dotenv').config()

const { CMS_API_URL } = process.env

const cmsApi = axios.create({
  baseURL: CMS_API_URL + '/api/'
})

function unsetCookie(res) {
  res.clearCookie(COOKIE_NAME)
}

function setCookie(res, token) {
  res.cookie(COOKIE_NAME, token, { httpOnly: true })
}

function errorResponse(res, error) {
  if (
    error.response?.data?.error?.status === 401
    || error.response?.data?.error?.status === 403
    || error.status === 401
    || error.status === 403
  ) {
    unsetCookie(res)
    return res.status(401).send('Deauthed')
  }

  if (error.response?.data?.error?.status) {
    return res.status(error.response?.data?.error?.status).send(error.response?.data?.error?.message)
  }

  if (error.status) {
    return res.status(error.status).send(error.message)
  }

  return res.status(400).send(error.message)
}

app.post('/authenticate/signup', async (req, res) => {
  const { body } = req
  const {
    firstName,
    lastName,
    email,
    password,
    confirmPassword,
  } = body

  if (confirmPassword !== password) {
    return res.status(400).send('Password\'s do not match')
  }

  try {
    const { data } = await cmsApi.post('auth/local/register', {
      username: email,
      email,
      password,
      firstName,
      lastName,
    })

    setCookie(res, data.jwt)

    res.send(data.user)
  } catch (error) {
    console.dir(error)
    errorResponse(res, error)
  }
})

app.post('/authenticate/login', async (req, res) => {
  const { body } = req
  const {
    email,
    password,
  } = body

  try {
    const { data } = await cmsApi.post('auth/local', {
      identifier: email,
      password,
    })

    setCookie(res, data.jwt)

    res.send(data.user)
  } catch (error) {
    console.log(error)
    errorResponse(res, error)
  }
})

app.get('/deauthenticate', (req, res) => {
  unsetCookie(res)
  res.send('Deauthed')
})

app.post('/*', async (req, res) => {
  const { url, cookies, body } = req
  const { [COOKIE_NAME]: token } = cookies
  try {
    const { data } = await cmsApi.post(url, body, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    res.send(data)
  } catch (error) {
    console.log(error)
    errorResponse(res, error)
  }
})

app.put('/*', async (req, res) => {
  const { url, cookies, body } = req
  const { [COOKIE_NAME]: token } = cookies
  try {
    const { data } = await cmsApi.put(url, body, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    res.send(data)
  } catch (error) {
    console.log(error)
    errorResponse(res, error)
  }
})

app.delete('/*', async (req, res) => {
  const { url, cookies } = req
  const { [COOKIE_NAME]: token } = cookies
  try {
    const { data } = await cmsApi.delete(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    res.send(data)
  } catch (error) {
    console.log(error)
    errorResponse(res, error)
  }
})

app.get('/*', async (req, res) => {
  const { url, cookies } = req
  const { [COOKIE_NAME]: token } = cookies
  try {
    const { data } = await cmsApi(url, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    res.send(data)
  } catch (error) {
    console.log(error)
    errorResponse(res, error)
  }
})

module.exports = app
