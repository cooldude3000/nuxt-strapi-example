export default [
  {
    volume: 0,
    sections: [
      {
        text: 'Section 0 - Introduction of Manual System',
        value: 0,
      },
      {
        text: 'Section 1 - Model Contract Document for Highway Works',
        value: 1,
      },
      {
        text: 'Section 2 - Implementing Standards',
        value: 2,
      },
      {
        text: 'Section 3 - Advice Notes',
        value: 3,
      },
    ],
  },
  {
    volume: 3,
    sections: [
      {
        text: 'Section 1 - Carriageway and Other Details',
        value: 1,
      },
      // {
      //   text: 'Section 2 - Safety Fences and Barriers (Not used)',
      //   value: 2,
      // },
      {
        text: 'Section 3 - National Motorway Communications System Installation Drawings',
        value: 3,
      },
    ],
  },
  {
    volume: 4,
    sections: [
      {
        text: 'Section 1 - Method of Measurement for Highway Works',
        value: 1,
      },
      {
        text: 'Section 2 - Notes for Guidance on the Method of Measurement for Highway Works',
        value: 2,
      },
      {
        text: 'Section 3 - Library of Standard Item Descriptions for Highway Works',
        value: 3,
      },
    ],
  },
  {
    volume: 5,
    sections: [
      {
        text: 'Section 1 - Geodetic Surveys',
        value: 1,
      },
      // {
      //   text: 'Section 2 - Maintenance Painting of Steel Highway Structures (Not Used)',
      //   value: 2,
      // },
      {
        text: 'Section 3 - Ground Investigation',
        value: 3,
      },
      {
        text: 'Section 7 - Mechanical and Electrical Installations in Road Tunnels, Movable Bridges and Bridge Access Gantries',
        value: 7,
      },
      {
        text: 'Section 8 - Trenchless Installation of Highway Drainage and Service Ducts',
        value: 8,
      },
      {
        text: 'Section 8 - Model Contract Documents For CCTV Survey of Highway Drainage Systems',
        value: 9,
      },
    ],
  },
]