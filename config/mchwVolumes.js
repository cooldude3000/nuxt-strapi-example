export default [
  {
    text: 'Volume 0 - Manual Contract Document for Major Works and Implementation Requirements',
    value: 0,
  },
  {
    text: 'Volume 1 - Specification for Highway Works',
    value: 1,
  },
  {
    text: 'Volume 2 - Notes for Guidance on the Specification for Highway Works',
    value: 2,
  },
  {
    text: 'Volume 3 - Highway Construction Details',
    value: 3,
  },
  {
    text: 'Volume 4 - Bills of Quantities for Highway Works',
    value: 4,
  },
  {
    text: 'Volume 5 - Contract Documents for Specialist Activities',
    value: 5,
  },
  {
    text: 'Volume 6 - Departmental Standards and Advice Notes on Contract Documentation and Site Supervision',
    value: 6,
  },
]