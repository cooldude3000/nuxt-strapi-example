export const DMRB = 'DMRB'
export const IAN = 'IAN'
export const MCHW = 'MCHW'
export const MCHW_NEW = 'MCHW_NEW'

const suiteOptions = [
  {
    text: DMRB,
    value: DMRB,
    key: DMRB,
  },
  {
    text: IAN,
    value: IAN,
    key: IAN,
  },
  {
    text: MCHW,
    value: MCHW,
    key: MCHW,
  }
]

export const downloadOptions = [
  'DMRB',
  'MCHW',
  'TSS',
  'IAN'
]

export const navStandards = [
  {
    text: 'DMRB',
    link: '/dmrb',
  },
  {
    text: 'MCHW',
    link: '/mchw',
  },
  {
    text: 'OTS',
    link: 'https://highways.sharepoint.com/sites/OperationalTechnologySpecifications?e=1%3A1b9d7aee0f144b1b81cf19e96f52ef57',
  },
  {
    text: 'IANs',
    link: '/search?q=&pageNumber=1&suite=IAN',
  },
]

export default suiteOptions
