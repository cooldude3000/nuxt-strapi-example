import { ARCHIVE_SEARCH_PATH, SEARCH_PATH, HTML_DOC_PATH } from './paths'

export default {
  [`/archive`]: { text: 'Archive' },
  [ARCHIVE_SEARCH_PATH]: { text: 'Archive Search Results' },
  [SEARCH_PATH]: { text: 'Search Results' },
  [HTML_DOC_PATH]: { text: 'HTML' },
  [`/help`]: { text: 'Help & Support' },
  [`/contact`]: { text: 'Contact' },
  [`/updates`]: { text: 'Latest Updates' },
  [`/document`]: { text: 'Document', active: true },
  [`/sign-in`]: { text: 'Sign in', active: true },
  [`/create-account`]: { text: 'Create account', active: true },
  [`/my-account`]: { text: 'My account', active: true },
  [`/your-details`]: { text: 'Your details', active: true },
  [`/my-account/download-standards`]: { text: 'Download Standards', active: true },
}
