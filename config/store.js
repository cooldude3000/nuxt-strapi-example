export const HIE_STORE = 'HIE_Store'
export const SYNCED_STATE_PROPERTIES = [
  'signedIn',
  'user',
  'documents',
  'savedDocuments',
  'recentlyViewedDocuments',
]

export const INITIAL_STATE = {
  signedIn: false,
  user: {
    id: null,
    username: '',
    email: '',
    provider: '',
    confirmed: null,
    blocked: null,
    createdAt: '',
    updatedAt: '',
    firstName: '',
    lastName: '',
  },
  savedDocuments: null,
  documents: null,
  recentlyViewedDocuments: [],
  searchResults: { total: 0, page: [] },
}
