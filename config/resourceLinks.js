import updatesIcon from '../assets/icons/documentupdates-icon.svg?inline'
import indexIcon from '../assets/icons/documentindex-icon.svg?inline'
import archiveIcon from '../assets/icons/documentarchive-icon.svg?inline'
import helpIcon from '../assets/icons/dmrbhelp-icon.svg?inline'
import userAccountIcon from '../assets/icons/account-icon.svg?inline'

const icons = {
  updatesIcon,
  indexIcon,
  archiveIcon,
  helpIcon,
  userAccountIcon
}

export const resourceLinks = [
  {
    title: 'Document updates',
    link: '/updates',
    icon_name: 'updatesIcon',
    dynamic: true,
    updatedContent: {
      text: 'A new publication is available',
    },
  },
  {
    title: 'Account',
    link: '/my-account',
    icon_name: 'userAccountIcon',
    dynamic: false,
    updateContent: {},
  },
  {
    title: 'Help',
    link: '/help',
    icon_name: 'helpIcon',
    dynamic: false,
    updateContent: {},
  },
]

export function resourceIcon(name) {
  return icons[name] || null
}
