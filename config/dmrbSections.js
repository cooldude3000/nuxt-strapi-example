export default [
  {
    text: 'Section 0',
    value: 0,
  },
  {
    text: 'Section 1',
    value: 1,
  },
  {
    text: 'Section 2',
    value: 2,
  },
  {
    text: 'Section 3',
    value: 3,
  },
  {
    text: 'Section 4',
    value: 4,
  },
  {
    text: 'Section 5',
    value: 5,
  },
  {
    text: 'Section 6',
    value: 6,
  },
]