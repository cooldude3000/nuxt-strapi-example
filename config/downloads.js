export const DOWNLOAD_VALID_IN_DAYS = 7

export const downloadState = {
  PROCESSING: 'PROCESSING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
}

