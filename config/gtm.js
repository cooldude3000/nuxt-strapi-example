// These are all the Variables defined in GTM
export const variables = {
  ANALYTICS_STORAGE: 'analytics_storage', // DLV "analytics_storage"
  SAVED_DOCUMENT_ID: 'savedDocumentId', // DLV "saved_document_id"
  CLICK_TEXT: 'clickText', // DLV "click_text"
  CONSENT: 'consent', // DLV "consent"
  DATE: 'date', // DLV "date"
  DOC_NAME: 'docName', // DLV "doc_name"
  DOC_REVISION: 'docRevision', // DLV "doc_revision"
  DOC_STANDARD: 'docStandard', // DLV "doc_standard"
  DOC_TYPE: 'docType', // DLV "doc_type"
  ERROR_MESSAGE: 'errorMessage', // DLV "error_message"
  FORM_ID: 'formId', // DLV "form_id"
  ISSUED: 'issued', // DLV "issued"
  PAGE_TYPE: 'pageType', // DLV "page_type"
  SEARCH_MATCH: 'searchMatch', // DLV "search_match"
  SEARCH_TERM: 'searchTerm', // DLV "search_term"
  VERSION_NUMBER: 'versionNumber', // DLV "version_number"
}

export const events = {
  SEARCH_TERM: 'Search Term',
  DOWNLOAD_TRACKING: 'Download',
  SAVED_DOCUMENTS: 'Saved Documents',
  ERROR_MESSAGE: 'Error Message',
  SIGN_IN: 'Sign In',
  PAGE_TYPE: 'Page Type',
  STANDARDS_DOWNLOAD: 'Standards Download',
  DOCUMENT_OPENED: 'Document Opened',
  DOCUMENT_DOWNLOAD: 'Document Download',
}
