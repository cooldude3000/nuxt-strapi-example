import { INITIAL_STATE } from '../config/store.js'

export default (store) => {
  store.subscribe(({ type, payload }) => {

    if (type === 'signedIn' && !payload.signedIn) {
      store.commit('user', { user: INITIAL_STATE.user })
      store.commit('savedDocuments', { savedDocuments: INITIAL_STATE.savedDocuments })
      store.commit('documents', { documents: INITIAL_STATE.documents })
    }

  })
}
