import { INITIAL_STATE, HIE_STORE } from '../config/store.js'

export function setStateToLocalStore(state, props) {
  localStorage.setItem(HIE_STORE, JSON.stringify(
    props.reduce((acc, key) => {
      return {
        ...acc,
        [key]: state[key]
      }
    }, {})
  ))
}

export function mergeStateWithLocalStore(state, props) {
  return {
    ...state,
    ...props.reduce((acc, key) => {
      return {
        ...acc,
        [key]: JSON.parse(localStorage?.getItem(HIE_STORE))?.[key] || INITIAL_STATE[key]
      }
    }, {})
  }
}

export const syncStorePlugin = props => store => {
  store.subscribe(({ type, payload }, state) => {

    if (type === 'signedIn' && !payload.signedIn) {
      localStorage.removeItem(HIE_STORE)
    } else {
      setStateToLocalStore(state, props)
    }

  })
}

export const replaceStateWithLocalStore = (store, props) => {
  store.replaceState(mergeStateWithLocalStore(store.state, props))
}
