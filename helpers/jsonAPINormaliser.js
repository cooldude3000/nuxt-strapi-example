export const isArrayLikeObject = val => !!(typeof val?.length === 'number' && !(val instanceof String))

function addId(data) {
  return {
    ...(data.id ? { id: data.id } : {}),
    ...(data.attributes || data),
  }
}

// This is normaliser to remove all "data" and "attribute"
// props from the json response.
// See https://jsonapi.org/ for details on the response shape.
export const jsonAPINormaliser = value =>
  Object.entries(value).reduce((acc, [key, val]) => {
    if (key === 'data' && val?.attributes) {
      return jsonAPINormaliser(addId(val))
    }

    if (val?.data?.attributes) {
      return {
        ...acc,
        [key]: jsonAPINormaliser(addId(val.data)),
      }
    }

    if (isArrayLikeObject(val?.data)) {
      return {
        ...acc,
        [key]: val?.data.map(dat => jsonAPINormaliser(addId(dat))),
      }
    }

    if (val?.data === null) {
      return {
        ...acc,
        [key]: null,
      }
    }

    if (val instanceof Object) {
      return {
        ...acc,
        [key]: jsonAPINormaliser(addId(val)),
      }
    }

    return {
      ...acc,
      [key]: val,
    }
}, {})


