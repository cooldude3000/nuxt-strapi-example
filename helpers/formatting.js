import get from 'lodash.get'
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

import { lifecyclesLut } from '~/config/lifecycles'
import { disciplinesLut } from '~/config/disciplines'
import { INPUT_DATE_FORMAT, DISPLAY_DATE_FORMAT } from '~/config/site'
import { DOWNLOAD_VALID_IN_DAYS, downloadState } from '~/config/downloads'
import { downloadOptions } from '~/config/suites.js'

export function processDocumentData(apiData) {
  return {
    document: {
      ...apiData,
      lifecycleStageFormatted: lifecycleStageFormatted(apiData.LIFECYCLE_STAGE),
      disciplineFormatted: disciplineFormatted(apiData.DISCIPLINE),
      issueDateFormatted: issueDateFormatted(apiData.DATE_OF_ISSUE),
      withdrawnDateFormatted: withdrawnDateFormatted(apiData.DATE_OF_WITHDRAWAL),
      attachmentUrl: attachmentUrl(apiData.ATTACHMENTS, apiData.NODE_ID),
      isWithdrawn: isWithdrawn(apiData.DATE_OF_WITHDRAWAL),
      hasHtml: apiData.ATTACHMENTS.some((entry) => entry.includes('text/html')),
    },
  }
}

export function lifecycleStageFormatted(LIFECYCLE_STAGE) {
  return get(
    lifecyclesLut,
    LIFECYCLE_STAGE,
    LIFECYCLE_STAGE
  )
}

export function disciplineFormatted(DISCIPLINE) {
  return get(
    disciplinesLut,
    DISCIPLINE,
    DISCIPLINE
  )
}

export function issueDateFormatted(DATE_OF_ISSUE) {
  return DATE_OF_ISSUE
    ? dayjs(DATE_OF_ISSUE).format(DISPLAY_DATE_FORMAT)
    : undefined
}

export function withdrawnDateFormatted(DATE_OF_WITHDRAWAL) {
  return DATE_OF_WITHDRAWAL
    ? dayjs(DATE_OF_WITHDRAWAL).format(DISPLAY_DATE_FORMAT)
    : undefined
}

export function attachmentUrl(ATTACHMENTS, NODE_ID) {
  return ATTACHMENTS.some(str => str.includes('text/html'))
    ? `/search/html/${NODE_ID}`
    : ATTACHMENTS.some(str => str.includes('application/pdf'))
    ? `/tses/attachments/${NODE_ID}`
    : undefined
}

export function isWithdrawn(DATE_OF_WITHDRAWAL) {
  return DATE_OF_WITHDRAWAL
    ? dayjs(DATE_OF_WITHDRAWAL).isBefore(dayjs())
    : false
}

export function sortByCreatedAtDesc(a, b) {
  if (dayjs(a.createdAt).isBefore(dayjs(b.createdAt))) return 1
  if (dayjs(b.createdAt).isBefore(dayjs(a.createdAt))) return -1
  return 0
}

export function getTitleAndReference(document) {
  return [document.DOCUMENT_REFERENCE, document.TITLE].filter(Boolean).join(' - ')
}

export function downloadRequestDataFormatted({
  userId,
  validAt,
  standards
}) {
  dayjs.extend(customParseFormat)

  return {
    userId,
    validAt: dayjs(validAt, INPUT_DATE_FORMAT).format(),
    params: {
      query: standards
        .map(str => 'SUITE = \'' + str + '\'').join(' | '),
    },
  }
}

export function downloadValues(completeDate) {
  const dayTodayDate = dayjs()
  const completeDateDate = dayjs(completeDate)
  const daysSinceCompleted = dayTodayDate.diff(completeDateDate, 'day')
  const downloadIsValid = daysSinceCompleted < DOWNLOAD_VALID_IN_DAYS

  return { downloadIsValid, daysSinceCompleted }
}

export function downloadAction({ completeDate, state, zipUrl }) {
  if (state === downloadState.PROCESSING) return 'Processing'
  if (state === downloadState.ERROR) return 'Failed'
  if (!zipUrl) return 'Failed'
  const { downloadIsValid } = downloadValues(completeDate)
  return downloadIsValid
    ? 'Download'
    : 'Expired'
}

export function formatDownloadValid({ completeDate, state }) {
  if (state === downloadState.PROCESSING) return ''
  if (state === downloadState.ERROR) return ''
  const { downloadIsValid, daysSinceCompleted } = downloadValues(completeDate)
  return downloadIsValid
    ? (DOWNLOAD_VALID_IN_DAYS - daysSinceCompleted).toString() + ' days'
    : 'Expired'
}

export function formatAsAt({ validAt }) {
  return validAt && dayjs(validAt).format(INPUT_DATE_FORMAT)
}

export function formatStandards({ searchParams }) {
  const { query } = searchParams
  if (!query) return
  return query
    .match(RegExp(downloadOptions.join('|'), 'g'))
    .filter(Boolean)
    .join(', ')
}
