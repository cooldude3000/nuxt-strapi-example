import get from 'lodash.get'
import dayjs from 'dayjs'
import { disciplinesLut } from '~/config/disciplines'
import { lifecyclesLut } from '~/config/lifecycles'
import { DISPLAY_DATE_FORMAT, OUTPUT_DATE_FORMAT } from '~/config/site'

const searchRegex = /([a-zA-z]{2}\d+){1}/g // two alpha characters follwed by a digit

const resultColumns = [
  'SUITE',
  'TITLE',
  'DISCIPLINE',
  'LIFECYCLE_STAGE',
  'REVISION',
  'DATE_OF_ISSUE',
  'DATE_OF_WITHDRAWAL',
  'HAS_PUBLIC_DOCUMENT',
  'DOCUMENT_REFERENCE',
  'VOLUME',
  'ATTACHMENTS',
  'SECTION',
]
const formatSearch = (input) => {
  const search = input.search(searchRegex)
  if (search === -1) return input

  const splicedVal =
    input.slice(search, search + 2) +
    ' ' +
    input.slice(search + 2, input.length)

  return splicedVal
}

const getSearchTermPart = (rawTerm) => {
  if (!rawTerm) return null
  const formattedSearchTerm = formatSearch(rawTerm)

  if (rawTerm === formattedSearchTerm) {
    return `(TITLE ~ '"${rawTerm}"' | DOCUMENT_REFERENCE ~ '"${rawTerm}"')`
  } else {
    return `(TITLE ~ '"${rawTerm}"' | DOCUMENT_REFERENCE ~ '"${rawTerm}"' | DOCUMENT_REFERENCE ~ '"${formattedSearchTerm}"')`
  }
}

const prepareSearchResults = (rawSearchResults, isArchive) => {
  const preparedSearchResults = {
    ...rawSearchResults,
    page: rawSearchResults.page.map((item) => ({
      ...item,
      formattedData: {
        SUITE: item.resultData.SUITE,
        DATE_OF_ISSUE: dayjs(item.resultData.DATE_OF_ISSUE).format(
          DISPLAY_DATE_FORMAT
        ),
        DATE_OF_WITHDRAWAL: item.resultData.DATE_OF_WITHDRAWAL
          ? dayjs(item.resultData.DATE_OF_WITHDRAWAL).format(
              DISPLAY_DATE_FORMAT
            )
          : undefined,
        IS_WITHDRAWN: item.resultData.DATE_OF_WITHDRAWAL
          ? dayjs(item.resultData.DATE_OF_WITHDRAWAL).isBefore(dayjs())
          : false,
        LIFECYCLE_STAGE: get(
          lifecyclesLut,
          item.resultData.LIFECYCLE_STAGE,
          item.resultData.LIFECYCLE_STAGE
        ),
        DISCIPLINE: get(
          disciplinesLut,
          item.resultData.DISCIPLINE,
          item.resultData.DISCIPLINE
        ),
        TITLE: [item.resultData.DOCUMENT_REFERENCE, item.resultData.TITLE]
          .filter(Boolean)
          .join(' - '),
        HTML_DOCUMENT_LINK: hasHtml(item.resultData.ATTACHMENTS)
          ? htmlDocumentLink(item.nodeId, item.resultData.SUITE)
          : undefined,
        // Download PDF link
        ATTACHMENT_URL: hasPdf(item.resultData.ATTACHMENTS)
          ? attachmentUrl(item.nodeId)
          : undefined,
        DOCUMENT_PAGE_URL: `/${isArchive ? 'archive/search' : 'search'}/${item.nodeId}`,
        // Inline PDF Button link
        INLINE_DOCUMENT_LINK: item.resultData.ATTACHMENTS.some(str => str && /^application\/pdf/.test(str))
          ? attachmentUrl(item.nodeId) + '?inline=true'
          : undefined,
        HAS_HTML: hasHtml(item.resultData.ATTACHMENTS),
      },
    })),
  }
  return preparedSearchResults
}

export function hasPdf(ATTACHMENTS) {
  return ATTACHMENTS.some(att => att.includes('application/pdf'))
}

export function attachmentUrl(nodeId) {
  return `/tses/attachments/${nodeId}`
}

export function htmlDocumentLink(nodeId, standard) {
  return `/search/html/${nodeId}?=standard=${standard}`
}

export function hasHtml(ATTACHMENTS) {
  return ATTACHMENTS.some((entry) => entry.includes('text/html'))
}

const buildQuery = ({
  searchTerm,
  discipline,
  lifecycles,
  dmrbVolume,
  mchwVolume,
  dmrbSection,
  mchwSection,
  suites,
}) => {
  const searchTermPart = getSearchTermPart(searchTerm)

  /**
   * DMRB/IANs specific query options
   * IANs documents are technically a subset of DMRB and will be using the same set of filters
   */
  let dmrbQuery = null
  let iansQuery = null

  if (suites.includes('DMRB') || suites.includes('IAN')) {
    const disciplinePart = discipline ? `DISCIPLINE = '${discipline}'` : null

    const lifecyclesPart = lifecycles
      ? `(${lifecycles
          .split(',')
          .map((part) => `LIFECYCLE_STAGE = '${part}'`)
          .join(' | ')})`
      : null

    const dmrbVolumePart = dmrbVolume ? `VOLUME = ${dmrbVolume}` : null
    const dmrbSectionPart = dmrbSection ? `SECTION = ${dmrbSection}` : null

    const queryParts = [
      disciplinePart,
      lifecyclesPart,
      dmrbVolumePart,
      dmrbSectionPart,
    ]
      .filter(Boolean)
      .join(' & ')

    if (suites.includes('DMRB')) {
      const suitePart = 'SUITE = DMRB'
      dmrbQuery = `(${[suitePart, searchTermPart, queryParts]
        .filter(Boolean)
        .join(' & ')})`
    }

    if (suites.includes('IAN')) {
      const suitePart = 'SUITE = IAN'
      iansQuery = `(${[suitePart, searchTermPart, queryParts]
        .filter(Boolean)
        .join(' & ')})`
    }
  }

  /** MCHW specific query options */
  let mchwQuery = null
  if (suites.includes('MCHW')) {
    const mchwVolumePart = mchwVolume ? `VOLUME = ${mchwVolume}` : null
    const mchwSectionPart = mchwSection ? `SECTION = ${mchwSection}` : null
    const queryParts = [
      mchwVolumePart,
      mchwSectionPart,
    ]
      .filter(Boolean)
      .join(' & ')

    const suitePart = 'SUITE = MCHW'

    mchwQuery = `(${[suitePart, searchTermPart, queryParts].filter(Boolean).join(' & ')})`
  }

  return [dmrbQuery, iansQuery, mchwQuery].filter(Boolean).join(' | ')
}

const prepareDateForApi = (dateString, incomingFormat = OUTPUT_DATE_FORMAT) => {
  if (!dateString) return null
  const parsedDate = dayjs(dateString, incomingFormat)
  return parsedDate.isValid() ? `${parsedDate.unix()}000` : '' // Fallback to empty string (today)
}

const queryFromNodeIds = (nodeIds) =>
  nodeIds.map((id) => `NODE_ID = '${id}'`).join(' | ')

export {
  buildQuery,
  prepareSearchResults,
  prepareDateForApi,
  resultColumns,
  queryFromNodeIds,
}
